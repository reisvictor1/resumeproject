const express = require('express')
const multer = require('multer')
const router = express.Router()
const resumeController = require('../controllers/resume')

const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,'../uploads')
    },
    filename: (req,res,cb) => {
        cb(null, new Date().toISOString() + req.body.imageName)
    }
})

const upload = multer({ storage: storage })

router.post('/:id/resume',upload.single('imageFile'), resumeController.uploadResume)

router.get('resume/:id', resumeController.getOneResume)

router.get('/resume', resumeController.getResumes)


module.exports = router