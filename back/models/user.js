const mongoose = require('mongoose')

const userSchema = mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    resumes : [{
        resume: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Resume"
        }
    }]

})


module.exports = mongoose.model('User', userSchema)