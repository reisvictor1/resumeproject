const mongoose = require('mongoose')

const resumeSchema = mongoose.Schema({

    discipline: {
        type: String,
        required: true
    },
    imageName:{
        type: String,
        required: true
    },
    imageFile: {
        type: String,
    }
})

module.exports =  mongoose.model('Resume', resumeSchema)