const multer = require('multer')
const resumeModel = require('../models/resume')
const userModel = require('../models/user')
const fs = require('fs')

module.exports.getResumes = async (req,res) => {

    const resumes = await resumeModel.find()

    res.json(resumes)

}


module.exports.getOneResume = async (req,res) => {

    const resume = await resumeModel.findOne({ _id: req.params.id })

    res.json(resume)

}

module.exports.uploadResume = async (req,res) => {

    let user = await (await userModel.findOne({ _id: req.params.id })).populate('resumes')

    console.log(req.file)

    let newResume = new resumeModel()

   

    newResume.discipline = req.body.discipline
    newResume.imageName = req.body.imageName
    newResume.imageFile = req.file.path

    const resumeSaved = await newResume.save()

    user.resumes.push(resumeCreated)
    user.save((error, resumeSaved) => {
        if(error){
            console.log('Resumo não pode ser salva.')
        }
        console.log('Resumo foi salva! ' + resumeSaved)
    })

    res.json(resumeCreated)


    res.json(resumeSaved)

}

module.exports.deleteResume = async (req,res) => {



    let resume = await resumeModel.findOne({_id: req.params.id})

    const userId = resume.user._id

    userModel.findOne({ _id: userId }).then( async (user) => {

        await user.resumes.pull({ _id: req.params.id })
        console.log(user.resumes)
        await user.save()
    })

    resume = await resumeModel.findOneAndDelete({ _id: req.params.id })

    res.json(resume)

}