const express = require('express')
const mongoose = require('mongoose')
const bodyparser = require('body-parser')



const PORT = process.env.PORT || 5000

mongoose.connect('mongodb+srv://resumos:resumeme@cluster0-fzxnn.mongodb.net/test?retryWrites=true&w=majority',  { useNewUrlParser: true, useCreateIndex: true, useFindAndModify: false, useUnifiedTopology:true })

const userRoute = require('./routes/user')
const resumeRoute = require('./routes/resume') 

const app = express()

app.use(bodyparser.json())
app.use(bodyparser.text())
app.use(bodyparser.urlencoded({ extended: true }))

app.use(express.static('uploads'))
app.use(userRoute)
app.use(resumeRoute)

app.listen(PORT, () => {
    console.log(`Ouvindo em http://localhost:${PORT}.`)
})